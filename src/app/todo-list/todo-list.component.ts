import { Component, OnInit } from '@angular/core';
import { NotesService } from '../services/api';
@Component({
  selector: 'todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {
  _dobleB:any = "";
  todoItems:any = [];
  
  constructor(private notesService:NotesService) { 
    this.notesService.get().subscribe(
      (data)=>{
        for (let i = 0; i < data["length"]; i++) {
          var obj:Object = {};
          obj["id"] = data[i]["id"];
          obj["text"] = data[i]["text"];
          obj["isComplete"] = (data[i]["status"] == "0")? false: true;
          this.todoItems.push(obj); 
        }
      }
    )
  }

  set dobleB(value){
    this._dobleB = value.replace(/[a]/g, "_");;
  }

  get dobleB(){
    return this._dobleB;
  }

  ngOnInit() {
  }

  addTodoItem(e){
    if(e.keyCode != 13) return false;
    this.todoItems.push({
      text: e.target.value,/*this.dobleB,*/
      isComplete: false
    });
    e.target.value = "";
  }

  markComplete(index){
    this.todoItems[index]["isComplete"] = !this.todoItems[index]["isComplete"];
  }

}
