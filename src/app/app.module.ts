import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Type } from '@angular/core';

import { AppComponent } from './app.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { CalendarComponent } from './calendar/calendar.component';
import { NotesComponent } from './notes/notes.component';
import { DebtsComponent } from './debts/debts.component';
import { ContactsComponent } from './contacts/contacts.component';

//Módulo de formularios (para usar forms o simplemente ngModel)
import { FormsModule } from '@angular/forms';
//Módulo de Rutas e interfaz de rutas
import { RouterModule, Routes } from '@angular/router';
//Módulo para habilitar las peticiones http
import { HttpModule } from '@angular/http';
//Módulo de cliente para usar httl
import { HttpClientModule } from '@angular/common/http';
import { NotesService } from './services/notes/notes.service';

interface FreakingRoutes{
  path?: string;
  component?: any;
  name?: string;
  redirectTo?: string;
  pathMatch?: string;
}

export const appRoutes = [
  { path: '', redirectTo: '/todo', pathMatch: 'full'},
  { path: 'todo', component: TodoListComponent, name:"TODO List" },
  { path: 'calendar', component: CalendarComponent, name:"Calendar" },
  { path: 'notes', component: NotesComponent, name:"Notes" },
  { path: 'debts', component: DebtsComponent, name:"Debts" },
  { path: 'contacts', component: ContactsComponent, name:"Contacts" },
  { path: '**', component:  NotFoundComponent}
];
@NgModule({
  declarations: [
    AppComponent,
    TodoListComponent,
    NotFoundComponent,
    CalendarComponent,
    NotesComponent,
    DebtsComponent,
    ContactsComponent
  ],
  imports: [
    HttpModule,
    HttpClientModule,
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  providers: [NotesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
