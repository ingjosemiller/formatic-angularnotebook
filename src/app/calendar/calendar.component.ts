import { Component, OnInit } from '@angular/core';
import * as VanillaCallendar from "vanilla-js-calendar"

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    var elem = document.getElementById("myCalendar");
    var JSCalendar = VanillaCallendar.JSCalendar;
    var JSCalendarEvent = VanillaCallendar.JSCalendarEvent;
    var calendar = new JSCalendar(elem, { width:'full' }).init().render();
  }

}
